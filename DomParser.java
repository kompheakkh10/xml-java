import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class DomParser {
    public static void main(String[] args) throws Exception{
        /*get file xml*/
        File inputFile = new File("input.xml");
        /*create new DocumentBuilderFactory instance*/
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        /*create new documentBuilder*/
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        /*parse the xml file*/
        Document document = documentBuilder.parse(inputFile);
        /*get document element*/
        document.getDocumentElement().normalize();
        /*print the root of the element (node name)*/
        System.out.println("Root element : "+ document.getDocumentElement().getNodeName());

        /*get all the element of and then return the list of the node*/
        NodeList nodeList = document.getElementsByTagName("student");
        System.out.println("---------------------------------------");

        /*make the for loop for print all the element that have in the list of the node*/
        for (int temp = 0; temp < nodeList.getLength(); temp++){
            /*get node with index that start with the index 0*/
            Node node = nodeList.item(temp);
            /*print the current element with getNodeName method*/
            System.out.println("\nCurrent Element : "+ node.getNodeName());

            /*check if the node is an element node*/
            if (node.getNodeType() == Node.ELEMENT_NODE){
                /*convert node into element*/
                Element element = (Element) node;
                System.out.println("Student roll no : "+ element.getAttribute("rollno"));
                System.out.println("Student firstname : "+ element.getElementsByTagName("firstname").item(0).getTextContent());
                System.out.println("Student firstname : "+ element.getElementsByTagName("lastname").item(0).getTextContent());
                System.out.println("Student firstname : "+ element.getElementsByTagName("marks").item(0).getTextContent());
            }
        }
    }
}
